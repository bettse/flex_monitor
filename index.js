const sqlite3 = require('sqlite3').verbose();
const { spawn } = require('child_process');
const DECIMAL = 10;

// rtl_fm -f 929.565M -s 22050 | ./multimon-ng -t raw -a POCSAG512 -a POCSAG1200 -a POCSAG2400 -a FLEX -f alpha /dev/stdin

const rtl_command = ['rtl_fm', '-f', '929.565M', '-s', '22050']
const multimon_command = ['./multimon-ng', '-t', 'raw', '-e', '-u', '-a', 'FLEX', '-f', 'alpha', '-']

const db = new sqlite3.Database('./pager.sqlite');

db.serialize(function() {
  db.run(`CREATE TABLE IF NOT EXISTS messages (
		protocol TEXT,
		baud INTEGER,
		levels INTEGER,
		cycleno INTEGER,
		frameno INTEGER,
		capcode TEXT,
		long_address TEXT,
		groupmessage TEXT,
		typeNum INTEGER,
    typeDesc TEXT,
		contents TEXT
	)`);
});
const addMessage = db.prepare('INSERT INTO messages(protocol,baud,levels,cycleno,frameno,capcode,typeDesc,contents) VALUES (?,?,?,?,?,?,?,?)');

// TODO: spawn datasette

const rtl_fm = spawn(rtl_command[0], rtl_command.slice(1)); //, {stdio: ['ignore', stream, 'pipe']})
rtl_fm.stderr.on('data', (data) => {
	console.log('stderr:', data.toString());
});

rtl_fm.on('close', (code) => {
	console.log(`rtl_fm exited with code ${code}`);
});

const multimon = spawn(multimon_command[0], multimon_command.slice(1)); //, {stdio: [stream, 'pipe', 'pipe']});
rtl_fm.stdout.pipe(multimon.stdin);

// Store partial previous messages
var partial = '';

multimon.stdout.on('data', (data) => {
  // console.log('stdout:', data.toString());
	const message = partial + data.toString();
  if (message.startsWith('Enabled demodulators')) {
    return;
  }

  const lines = message.split('\n');
  // Final line might be from multimon-ng's flushing of stdout. Save last part and add to next message
  partial = lines.pop();

  //verbprintf(0, "FLEX|%i/%i|%02i.%03i.%c|%010" PRId64 "|%c%c|%1d|", flex->Sync.baud, flex->Sync.levels, flex->FIW.cycleno, flex->FIW.frameno, PhaseNo, flex->Decode.capcode, (flex->Decode.long_address ? 'L' : 'S'), (flex_groupmessage ? 'G' : 'S'), flex->Decode.type);
	//FLEX|3200/4|04.037.C|0007130878|LS|5|ALN|3.0.K|All clear code red psvmc, maint tower, lower level, kitchen ansul hood pc  [07]

  lines.forEach(line => {
    try {
      const parts = line.split('|');

      //TODO: confirm that all messages (even non-flex) use '|'

      const [protocol, baudlevel, cycleframe, capcode, ls, typeNum, typeDesc, ...rest] = parts

      const [baud, levels] = baudlevel.split('/');
      const [cycleno, frameno, phaseno] = cycleframe.split('.');


      switch(typeDesc) {
        case 'ALN':
          const [fragmentation, contents] = rest;
          const [frag, cont, frag_flag] = fragmentation.split('.');
          // cont: 1-bit Message Continued Flag—When set to 1, this flag indicates fragments of this message are to be expected in any or possibly all of the following frames until a fragment with C = 0 is found. The longest message that fits into a frame is eighty-four code words. Three alpha characters per word yields a maximum message of 252 characters in a frame, assuming no other traffic. Messages longer than this value must be sent as several fragments.
          // frag: 2-bit Message Fragment Number—This is a modulo 3 message fragment number that is incremented by 1 in successive message fragments. The initial fragment starts at 11 and each following fragment is incremented by 1 modulo 3, (11, 00, 01, 10, 00, 01, 10, 00, etc.). The 11 state (after the initial fragment) is skipped in this process to avoid confusion with the single fragment of a non-continued message. The final fragment is indicated by the Message Continued Flag being reset to 0.
          addMessage.run(protocol, parseInt(baud, DECIMAL), parseInt(levels, DECIMAL), parseInt(cycleno, DECIMAL), parseInt(frameno, DECIMAL), capcode, typeDesc, contents);
          break;
        case 'NUM':
        case 'TON':
        case 'BIN':
          addMessage.run(protocol, parseInt(baud, DECIMAL), parseInt(levels, DECIMAL), parseInt(cycleno, DECIMAL), parseInt(frameno, DECIMAL), capcode, typeDesc, rest);
          break;
        case 'UNK':
          console.log('skip', typeDesc, rest);
          break;
      }
    } catch(e) {
      console.log({line, e})
    }
  })
});

multimon.stderr.on('data', (data) => {
	console.log('stderr:', data.toString());
});

multimon.on('close', (code) => {
	console.log(`multimon exited with code ${code}`);
	db.close();
});

